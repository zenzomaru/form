<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Start Apprentice</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
		<link href="css/styles.css" rel="stylesheet" />
		<script type="text/javascript" src="js/jquery-3.4.1.min.js">
		</script>
		<script type="text/javascript" >
		$(document).ready(function(){
			  $("#ddd").click(function(){
			    
			  });
			});
		</script>
		
		
</head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="index.jsp">Start Apprentice</a><button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu <i class="fas fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="edit_time.jsp" >Edit Timestamp</a></li>
                        
                    </ul>
                </div>
            </div>
	    </nav>
		<section class="page-section" id="contact">
			<div class="container d-flex flex-column">
				<br>
				<!-- Contact Section Heading-->
				<h2
					class="page-section-heading text-center text-uppercase text-secondary mb-0">User</h2>
				<!-- Icon Divider-->
				<div class="divider-custom">
					<div class="divider-custom-line"></div>
					<div class="divider-custom-icon">
						<i class="fas fa-star"></i>
					</div>
					<div class="divider-custom-line"></div>
				</div>
				<!-- Contact Section Form-->
				<div class="row">
					<div class="col-lg-4 mx-auto">
						<!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
						<div class="form-group floating-label-form-group controls mb-0 pb-2">
							<table class="table">
								<thead class="thead-dark">
									<tr>
										<th scope="col">ID</th>
										<th scope="col">Username</th>
										<th scope="col">Password</th>
										<th scope="col">Name</th>
										<th scope="col">Delete</th>
									</tr>
								</thead>
								<tbody>
									<%@ include file="control/conn.jsp" %>	
									<%
									try {
										String sql = "SELECT * FROM user";
										ResultSet rec = s.executeQuery(sql);
									%>
									
									<%while((rec!=null) && (rec.next())) { %>
									  <tr>
									    <td><div align="center"><%=rec.getString("id")%></div></td>
									    <td><div align="center"><%=rec.getString("username")%></div></td>
									    <td align="right">xxxxxx</td>
									    <td><div align="center"><%=rec.getString("name")%></div></td>
									    <td><button type="button" class="btn btn-danger" onclick="window.location.href='control/del_time.jsp?id=<%=rec.getString("id")%>&table=user';">del</button></td>
									  </tr>
							       	<%}%>
							  	     
							    <%	
								} catch (Exception e) {
									// TODO Auto-generated catch block
									out.println(e.getMessage());
									e.printStackTrace();
								}
							
								try {
									if(s!=null){
										s.close();
										connect.close();
									}
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									out.println(e.getMessage());
									e.printStackTrace();
								}
							%>
								
								</tbody>
							</table>
							<button type="button" class="btn btn-success" data-toggle="modal"  data-target="#insertuser">Add</button>
						</div>
					</div>
					<br/>
				</div>
			</div>
		</section>
		<div class="modal fade" id="insertuser" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="sentuser" name="sentuser" novalidate="novalidate">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">ADD USER</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="control-group">
								<p class="lead mb-0" id="tel">Username</p>
								<input type="text" class="form-control" name="username"
									id="username" placeholder="Username"
									data-validation-required-message="Please enter your Username."
									required />
							</div>
							<div class="control-group">
								<p class="lead mb-0" id="tel">Password</p>
								<input class="form-control" id="password" name="password"
									type="password" placeholder="Password"
									data-validation-required-message="Please enter your password."
									required />
							</div>
							<div class="control-group">
								<p class="lead mb-0" id="tel">Name</p>
								<input class="form-control" id="name" name="name" type="text"
									placeholder="Name" required="required"
									data-validation-required-message="Please enter your name." />
	
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>
							<input class="btn btn-primary" id="smbtn" type="button"
								value="ตกลง">
						</div>
					</form>
				</div>
			</div>
		</div>
	<!-- Footer-->
        <footer class="footer text-center">
            <div class="container">
                <div class="row">
                    <!-- Footer Location-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Location</h4>
                        <p class="lead mb-0">555 Joey Zenzo<br />Khonkean</p>
                    </div>
                    <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4">Around the Web</h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="https:\\www.facebook.com/Joey.jirapong"><i class="fab fa-fw fa-facebook-f"></i></a><a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a><a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a><a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    <!-- Footer About Text-->
                    <div class="col-lg-4">
                        <h4 class="text-uppercase mb-4">About Zenzomaru </h4>
                        <p class="lead mb-0">Study KKU .</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © Zenzo Website 2020</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <script src="js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <!-- Contact form JS-->
        <script src="assets/mail/jqBootstrapValidation.js"></script>
        <script src="assets/mail/contact_me.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>